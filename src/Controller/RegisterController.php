<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegisterController extends AbstractController
{
    private $entityManager;

    private $userPasswordHasherInterface;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }
    
    #[Route('/register', name: 'app_register')]
    public function index(Request $request): Response
    {

        $user = new User();

        $form = $this->createForm(RegisterType::class, $user);
        // récupération du l'objet request au formulaire
        $form->handleRequest($request);

        //  si le formulaire es soumis et validé 
        if ($form->isSubmitted() && $form->isValid()) {
            // on récupére la data du form
            $user = $form->getData();

            // encodage du mot de passe que l'on recupere grace au geter
            $password = $this->userPasswordHasherInterface->hashPassword($user, $user->getPassword());

            // // envoi du mot de passe crypter grace au seter
            $user->setPassword($password);

            // ON ENVOIE la data en bdd
            $this->entityManager->persist($user);
            $this->entityManager->flush();

        }


        return $this->render('register/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
