<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Service\CallApiService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MovieDetailsController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/movie/{id}&{rate}&{numberVote}', name: 'app_movie_details')]
    public function index($id, $rate, $numberVote, Request $request, CallApiService $callApiService): Response
    {
        $user = $this->getUser();

        $oldComment = $this->entityManager->getRepository(Comment::class)->findBy([
            'movie' => $id
        ]);

        if (isset($user)){
            $comment = new Comment();

            $form = $this->createForm(CommentType::class, $comment);

            // récupération du l'objet request au formulaire
            $form->handleRequest($request);

            //  si le formulaire es soumis et validé
            if ($form->isSubmitted() && $form->isValid()) {
                // on récupére la data du form
                $comment = $form->getData();

                // ON ENVOIE la data en bdd
                $this->entityManager->persist($comment);
                $this->entityManager->flush();
            }
            return $this->render('movie_details/index.html.twig', [
                'movie' => $callApiService->getMovie($id),
                'id' => $id,
                'form' => $form->createView(),
                'comments' => $oldComment
            ]);
        }

        
        // $this->addFlash('success', 'Votre commentaire à bien été posté');



        return $this->render('movie_details/index.html.twig', [
            'movie' => $callApiService->getMovie($id),
            'id' => $id,
            'comments' => $oldComment
        ]);
    }
}
