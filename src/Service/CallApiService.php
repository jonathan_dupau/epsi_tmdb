<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    private $client;

    private $page = 1;

    public function __construct(HttpClientInterface $client, SecretKey $secretKey)
    {
        $this->client = $client;
        $this->secretKey = $secretKey->getKey();
    }

    public function getTmdbData(): array
    {
        $response = $this->client->request(
            'GET',
            'https://api.themoviedb.org/3/movie/popular?page=1&api_key='.$this->secretKey
        );

        return $response->toArray();
    }
    public function getMovie($id): array
    {
        $response = $this->client->request(
            'GET',
            'https://api.themoviedb.org/3/movie/'.$id.'?api_key='.$this->secretKey
        );

        return $response->toArray();
    }
}
